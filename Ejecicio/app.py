from flask import Flask, jsonify, request, abort, make_response
from flask_cors import CORS
import json, datetime

# mensaje de pureba para la rama develop

app = Flask(__name__)
# cors = CORS(app)
cors = CORS(app, resources={r"/todo/*": {"origins": "*"}})
app.config['CORS_HEADERS']= 'Content-Type'

notas = []

import random
import csv

valor_inicial = [3, 5, 6, 8, 7, 7, 9, 3]


def inicializar():
    """
    Funcion encargada de crear y devolver un individuo el cual se conforma por 3 numeros generados aleatoriamente
    entre los valores de -1 y 1
    :return: Individuo generado
    """
    numbers = []
    while len(numbers) < 3:
        number = random.uniform(-1, 1)
        numbers.append(number)

    return numbers


def poblar(num_individuos):
    """
    Funcion encargada de crear una poblacion conformada por varios indiviuos
    :param num_individuos: Cantidad de individuos que existiran en la poblacion
    :return: Lista de la poblacion con todos los individuos requeridos
    """
    poblacion = []

    for x in range(0, num_individuos):
        poblacion.append(inicializar())

    return poblacion


def evaluar(individuo, notas_cargadas):
    """
    Funcion encargada de realizar el calculo del fitness utilizando la formula de error cuadratico medio para poder
    determinar la presicion del individuo con respecto a las notas reales
    :param individuo: Individuo a evaluar
    :param notas_cargadas: Diccionario de notas reales obtenidas de la fuente de origen
    :return: Valor fitness calculado
    """
    notas_calculadas = []
    cantidad_notas = 0
    sumatoria = 0
    for nota in notas_cargadas:
        nota_calculada = individuo[0]*float(nota['P1']) + individuo[1]*float(nota['P2']) + individuo[2]*float(nota['P3'])
        factor = (float(nota['NF']) - nota_calculada) ** 2
        sumatoria = sumatoria + factor

        notas_calculadas.append(nota_calculada)
        cantidad_notas += 1
    factor = float(1.0/cantidad_notas)
    fitness = factor * sumatoria
    print('CALCULANDO FITNESS> total notas: '+str(cantidad_notas) +
    ' fitness:'+str(fitness) + ' sumatoria:'+ str(sumatoria) + 'factor:'+str(factor))
    return fitness


def seleccionar(poblacion, notas, tipo_seleccion):
    """
    Funcion encargada de seleccionar a los padres de una poblacion
    :param poblacion: Poblacion a evaluar
    :param notas: Diccionario de notas obtenidas del archivo de ingreso
    :param tipo_seleccion: Numero con el tipo de seleccion a utilizar para la seleccion de los padres de la poblacion
    1) Seleccion por mejor fitness 2) Seleccion random 3) Seleccion directa de padres en los indices 0,2,4,6,8
    :return: Padres seleccionados
    """
    poblacion_antigua = []
    for x in range(0, len(poblacion)):
        poblacion_antigua.append(poblacion[x])

    all_listado_fitness = []
    seleccion_fitness = []
    seleccion = []

    if tipo_seleccion == 1:
        ## SELECCION POR MEJOR FITNESS, VALOR MENOR
        for individuo in poblacion:
            fitness = evaluar(individuo, notas)
            all_listado_fitness.append(fitness)

        for x in range(0, 5):
            min_fitness = all_listado_fitness.index(min(all_listado_fitness))

            seleccion.append(poblacion[min_fitness])
            seleccion_fitness.append(all_listado_fitness[min_fitness])
            all_listado_fitness.pop(min_fitness)
            poblacion.pop(min_fitness)

        return seleccion
    elif tipo_seleccion == 2:
        for x in range(0, 5):
            pos = random.randrange(0, len(poblacion))
            seleccion.append(poblacion[pos])
            poblacion.pop(pos)

        return seleccion

    elif tipo_seleccion == 3:
        seleccion = [poblacion[0], poblacion[2], poblacion[4], poblacion[6], poblacion[8]]
        return seleccion

    return None




def emparejar(seleccion):
    """
    Funcion utilizada para emparejar los padres con los hijos
    :param seleccion: Todos los hijos y padres seleccionados
    :return: Resultado dado despues del emparejamiento y del cruce
    """

    hijo_1 = cruzar(seleccion[0], seleccion[1])
    hijo_2 = cruzar(seleccion[2], seleccion[3])
    hijo_3 = cruzar(seleccion[0], seleccion[2])
    hijo_4 = cruzar(seleccion[3], seleccion[4])
    hijo_5 = cruzar(seleccion[0], seleccion[4])

    nuevos_hijos = [hijo_1, hijo_2, hijo_3, hijo_4, hijo_5]

    for hijo in nuevos_hijos:
        seleccion.append(mutar(hijo))

    return seleccion

def cruzar(padre_1, padre_2):
    """
    Funcion encargada de realizar los cruces entre padres e hijos utilizando la probabilidad del 50% para cada uno
    :param padre_1: Primer padre que se desea realizar el cruce
    :param padre_2: Segundo padre que se desea realizar el cruce
    """
    pos_0 = padre_1[0] if random.random() < 0.5 else padre_2[0]
    pos_1 = padre_1[1] if random.random() < 0.5 else padre_2[1]
    pos_2 = padre_1[2] if random.random() < 0.5 else padre_2[2]

    hijo = [pos_0, pos_1, pos_2]

    return hijo

def mutar(hijo):

    pos = random.randrange(0, len(hijo))
    factor = random.uniform(-0.3, 0.3)
    hijo[pos] = hijo[pos] + factor
    return hijo

def criterio(poblacion, notas, criterio, generacion):
    """
    Funcion que evalua la poblacion y aplica un criterio para indicar si cumple o no con lo esperado
    :param poblacion: Poblacion completa
    :param criterio: Numero de criterio de evaluacion los cuales pueden ser 1) Fitness promedio de la poblacion
    2) Numero maximo de generaciones 3) Fitness de un individuo que cumple con la calidad esperada
    :return: Si cumple con el criterio, devuelve al individuo de lo contrario regresa un valor nulo
    """
    num_maximo = 150
    fitness_promedio = 100
    fitness_individuo = 100
    if criterio == 1:
        calidad_total = 0
        for individuo in poblacion:
            calidad_total = calidad_total + evaluar(individuo, notas)

        calidad_promedio = calidad_total / len(poblacion)
        print('CALIDAD PROMEDIO - ' + str(calidad_promedio))
        if calidad_promedio <= fitness_promedio:
            pos = random.randrange(0, len(poblacion))
            return poblacion[pos]
    elif criterio == 2:
        if generacion >= num_maximo:
            pos = random.randrange(0, len(poblacion))
            return poblacion[pos]
    elif criterio == 3:
        for individuo in poblacion:
            calidad_obtenida = evaluar(individuo, notas)
            print('CALIDAD - ' + str(calidad_obtenida))
            if calidad_obtenida <= fitness_individuo:
                return individuo

    return None

@app.route('/todo/api/v1.0/notas/pr', methods=['GET'])
def predictor(notas, id_criterio, seleccion_padres):
    """
    Funcion principal para realizar la prediccion
    :param notas: Diccionario de notas completas junto con notas finales
    :param id_criterio: Criterio que se va a utilizar para la evaluacion
    :param seleccion_padres: Forma de seleccion de padres que se utilizara
    :return: Individuo seleccionado que cumple con el criterio establecido
    """
    print('NOTAS RECIBIDAS> '+ str(len(notas)) + ' CRITERIO> '+ str(id_criterio) + ' SELECCIO> ' + str(seleccion_padres))
    poblacion = poblar(10)
    generacion = 0

    fin = criterio(poblacion, notas, id_criterio, generacion)

    while fin is None:
        padres = seleccionar(poblacion, notas, seleccion_padres)
        poblacion = emparejar(padres)
        fin = criterio(poblacion, notas, id_criterio, generacion)
        print('GENERACION - ' + str(generacion))
        generacion += 1

    return {'generaciones': generacion, 'individuo': fin}

def actualizar_bitacora(nuevo_log):
    bitacora = []

    f = open("bitacora.txt", mode="r")
    lines = f.readlines()
    print(lines)
    bitacora = json.loads(str(lines[0]))
    bitacora.append(nuevo_log)

    print(json.dumps(bitacora))
    f.close()

    d = open("bitacora.txt", mode="w")
    d.write(json.dumps(bitacora))
    d.close()

@app.route('/ping')
def ping():
    return jsonify({"message": "Romulo Rosales"})

@app.route('/todo/api/v1.0/notas', methods=['GET'])
def get_nota():
#    return jsonify(notas)
    return jsonify('repuesta', 'mensaje')


@app.route('/todo/api/v1.0/prediccion', methods=['POST'])
# @cross_origins
def crear_prediccion():
    if not request.json or not 'CR' in request.json:
        abort(400)

    criterio = request.json['CR']
    seleccion = request.json['SL']
    finPredictor = predictor(notas,criterio,seleccion)

    # header['Access-Control-Allow-Origin'] = '*'
    response = jsonify({'predictor': finPredictor},{'nota': notas},{'CRITERIO':criterio},{'SELECION':seleccion})
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Headers", "*")
    response.headers.add("Access-Control-Allow-Methods", "*")
    return response, 201


@app.route('/todo/api/v1.0/cargarnota', methods=['POST'])
def create_nota():
    if not request.json or not 'NOTAS' in request.json:
        abort(400)
    filename = 'archivo.csv'
    notas_cargadas = request.json['NOTAS']
    criterio = int(request.json['CR'])
    seleccion = int(request.json['SL'])
    notas = []
    for nota in notas_cargadas:
        tmp = {'P1': nota['parcial1'], 'P2': nota['parcial2'],
        'P3': nota['parcial3'], 'NF': nota['notafinal']}
        notas.append(tmp)

    print('criterio - ' + str(criterio))
    # print(notas)
    finPredictor = predictor(notas,criterio,seleccion)
    # response = jsonify({'predictor': finPredictor},{'nota': notas},{'CRITERIO':criterio},{'SELECION':seleccion})
    response = jsonify({'predictor': finPredictor})
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Headers", "*")
    response.headers.add("Access-Control-Allow-Methods", "*")

    # tmp = {'P1': nota['P1'], 'P2': nota['P2'], 'P3': nota['P3'], 'NF': nota['NF']}
    # notas.append(nota)
    today = datetime.date.today()
    log = {'datetime': str(today.year) + '-' + str(today.month) + '-'+ str(today.day), 'filename': filename, 'generaciones': finPredictor['generaciones'],
    'individuos':finPredictor['individuo'], 'criterio':criterio, 'seleccion': seleccion}
    actualizar_bitacora(log)

    return response, 201



@app.route('/todo/api/v1.0/notabk', methods=['POST'])
def create_nota_bk():
    if not request.json or not 'P1' in request.json:
        abort(400)
    nota = {

        'P1': request.json['P1'],
        'P2': request.json['P2'],
        'P3': request.json['P3'],
        'NF': request.json['NF']
    }
    # tmp = {'P1': nota['P1'], 'P2': nota['P2'], 'P3': nota['P3'], 'NF': nota['NF']}
    notas.append(nota)
    return jsonify(nota), 201


@app.route('/todo/api/v1.0/bitacora', methods=['GET'])
def get_bitacora():
    bitacora = []

    f = open("bitacora.txt", mode="r")
    lines = f.readlines()
    print(lines)
    bitacora = json.loads(str(lines[0]))
    f.close()
    return jsonify(bitacora), 201


@app.route('/todo/api/v2.0/filtro', methods=['POST'])
# @cross_origins
def crear_filtro():
    if not request.json or not 'FR' in request.json:
        abort(400)

    frecuencia = request.json['FR']
    tipo = request.json['TP']
#    finPredictor = predictor(notas,criterio,seleccion)

    # header['Access-Control-Allow-Origin'] = '*'
#    response = jsonify({'predictor': finPredictor},{'nota': notas},{'CRITERIO':criterio},{'SELECION':seleccion})
    response = jsonify({'Frecuencia':frecuencia},{'Filtro':tipo})
#    response.headers.add("Access-Control-Allow-Origin", "*")
#    response.headers.add("Access-Control-Allow-Headers", "*")
#    response.headers.add("Access-Control-Allow-Methods", "*")
    return response, 201








@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':

    app.run(debug = True, port=4500)
